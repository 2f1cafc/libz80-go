Z80LIB_SRC_DIR=clib

GO=go

all: $(Z80LIB_SRC_DIR)/codegen/opcodes_table.h
	$(GO) build

$(Z80LIB_SRC_DIR)/codegen/opcodes_table.h:
	$(MAKE) -C $(Z80LIB_SRC_DIR)/codegen opcodes

run:
	go run main/main.go

clean:
#	$(MAKE) -C $(Z80LIB_SRC_DIR) clean
	$(GO) clean
