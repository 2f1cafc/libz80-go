#include "z80.h"

#include "_cgo_export.h"

byte z80_memIn(int param, ushort address) {
  return Z80DataMemIn(param, address);
}

byte z80_ioIn(int param, ushort address) {
  return Z80DataIoIn(param, address);
}

void z80_memOut(int param, ushort address, byte data) {
  return Z80DataMemOut(param, address, data);
}

void z80_ioOut(int param, ushort address, byte data) {
  return Z80DataIoOut(param, address, data);
}

ushort z80_getRegHL(Z80Context *ctx) {
  return ctx->R1.wr.HL;
}

void c_init(Z80Context *ctx) {
  ctx->memRead = z80_memIn;
  ctx->memWrite = z80_memOut;
  ctx->ioRead = z80_ioIn;
  ctx->ioWrite = z80_ioOut;
}
