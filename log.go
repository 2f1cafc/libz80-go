package libz80

import "fmt"

func logf(fmtStr string, args ...interface{}) {
	log(fmt.Sprintf(fmtStr, args...))
}

func log(msg string) {
	fmt.Println(msg)
}
