/* simple Go wrapper around libz80:
   https://github.com/ggambetta/libz80 */

package libz80

//#cgo  CFLAGS: -I${SRCDIR}/clib
//#include "z80.h"
//#include "wrapper.h"
import "C"

import (
	"fmt"
	"unsafe"
)

type RegW int
const (
	R_HL RegW = 3
)

type RegB int
const (
	R_A RegB = 1
)

type RegsSlice [7]uint16

const memSize = 0x10000 /* max address = 0xFFFF */

var mem []uint8
var ctx C.Z80Context

//export Z80DataMemIn
func Z80DataMemIn(param C.int, address C.ushort) C.uchar {
	logf("memory in %04X -> %02X", address, mem[address])
	return C.uchar(mem[address])
}

//export Z80DataMemOut
func Z80DataMemOut(param C.int, address C.ushort, data C.uchar) {
	mem[address] = uint8(data)
}

//export Z80DataIoIn
func Z80DataIoIn(param C.int, address C.ushort) C.uchar {
	logf("IO in %04X -> %02X", address, 0)
	return 0
}

//export Z80DataIoOut
func Z80DataIoOut(param C.int, address C.ushort, data C.uchar) {
}

func wRegs() *[7]uint16 {
	return (*[7]uint16)(unsafe.Pointer(&ctx.R1))
}

func bRegs() *[14]uint8 {
	return (*[14]uint8)(unsafe.Pointer(&ctx.R1))
}

func StateStr() string {
	return fmt.Sprintf("PC:%04X HL:%04X A:%X", ctx.PC, wRegs()[R_HL], bRegs()[R_A])
}

func Init() {
	mem = make([]uint8, memSize)

	C.c_init(&ctx)
}

func Exec() {
	C.Z80Execute(&ctx)
}

func MemWrite(addr uint16, data []uint8) {
	for i, b := range data {
		Z80DataMemOut(0, C.ushort(addr+uint16(i)), C.uchar(b))
	}
}
