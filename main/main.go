package main

import (
	"gitlab.com/2f1cafc/libz80"
	"fmt"
)

func main() {
	libz80.Init()
	libz80.MemWrite(0, []uint8{ 0x23 /* inc HL */ })
	fmt.Println(libz80.StateStr())
	libz80.Exec()
	fmt.Println(libz80.StateStr())
}
